<?php

namespace App\Http\Controllers;

use App\Models\Enderecos;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;

class MasterApiController extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    public function create()
    {
        //
    }

    public function store(Request $request)
    {
        
        if(empty($request->Cidade)  ||  empty($request->Logradouro)  ||  empty($request->Numero)
          ||  empty($request->Bairro))
        {
            $response = array();
            if(empty($request->Cidade))
                $response = ['Status'                  => 'Erro','Mensagem'                  => 'Campo Cidade vazio!'];
            else if(empty($request->Logradouro))
            {
                $response = ['Status'                  => 'Erro','Mensagem'=> 'Campo Logradouro vazio!'];  
            }
            else if(empty($request->Numero))
            {
                $response = ['Status'                  => 'Erro','Mensagem'=> 'Campo Numero vazio!'];  
            }
            else if(empty($request->Bairro))
            {
                $response = ['Status'                  => 'Erro','Mensagem'=> 'Campo Bairro vazio!'];  
            }
            else if(empty($request->Cidade) and empty($request->Logradouro)
            and empty($request->Numero) and empty($request->Bairro))
            {
                $response = ['Status'                  => 'Erro','Mensagem'=> 'Campos vazios!'];    
            }

            return response()->json($response, 401);
        }
        else{
            $data = $request->all();
            $data['Cidade'] = $request->Cidade;
            $data['Logradouro'] = $request->Logradouro;
            $data['Numero'] = $request->Numero;
            $data['Bairro'] = $request->Bairro;
            $resultado = DB::select('select * from enderecos where Cidade = ? and Logradouro = ? and Numero = ? and Bairro = ?', [$data['Cidade'],$data['Logradouro'],$data['Numero'],$data['Bairro']]);
            $cont = count($resultado);
            if($cont > 0)
            {
                $response = ['Status'=> 'Erro','Mensagem'=> 'Endereço já cadastrado!'];   
                return response()->json($response, 400);    
            }
            
                $this->enderecos->create($data);
                $response = array();
                $response = ['Status'=> 'Sucesso','Resultado'=> "Informações salvas!"];
                return response()->json($response, 200);
                    
        }

    }

    public function show($id)
    {
          
        $cont = strlen($id);
        if($cont > 2)
        {
            print_r($id);
            $resultado = $this->validarEstado($id);
            if(!$resultado)
            {
                $response = ['Status'=> 'Erro','Mensagem'=> 'Estado não existe!'];   
                return response()->json($response, 400); 
            }
            $request = $this->NomePorUf($id);  
                    
        }
        else
        {
            $resultado = $this->validarUF($id);
            if(!$resultado)
            {
                $response = ['Status'=> 'Erro','Mensagem'=> 'UF não existe!'];   
                return response()->json($response, 400); 
            }
            $request = $this->UfPorNome($id);
        }
        
        $request = $id;
        $response = $this->Cidade($request);
        if(empty($id)  ||  $response == 'erro')
        {
            $response = ['Status'=> 'Erro','Mensagem'=> 'Estado não encontrado!'];      
            return response()->json($response, 400); 
        }
        
        return $response;
       
    }

    public function edit($id)
    {
        //
    }

    public function update(Request $request, $id)
    {
        //
    }

    public function destroy($id)
    {
        //
    }

    public function ConsumoApi($uf)
    {     

        $url = "https://servicodados.ibge.gov.br/api/v1/localidades/estados/".$uf."/distritos";
            $curl = curl_init();
            curl_setopt_array($curl, array(
                             CURLOPT_URL                 => $url,
                             CURLOPT_RETURNTRANSFER                 =>true,
                             CURLOPT_ENCODING                 =>"",
                             CURLOPT_MAXREDIRS                 =>10,
                             CURLOPT_TIMEOUT                 =>0,
                             CURLOPT_FOLLOWLOCATION                 =>true,
                             CURLOPT_HTTP_VERSION                 =>CURL_HTTP_VERSION_1_1,
                             CURLOPT_CUSTOMREQUEST                 =>"GET",
                             CURLOPT_POSTFIELDS                 =>"",
                             CURLOPT_HTTPHEADER                 =>array(
                                                       "Content-Type: application/json",
                                                       "cache-control: no-cache",
                                                       ),
                                                       ));

                $response = curl_exec($curl);
                curl_close($curl);
                set_time_limit(200);        
        if($response != 'Error: Please enter a valid URL to shorten')
		return $response;
        else
        return false;
        
    }
/* 
    public function Estados()
    {
        $result = json_decode($this->ConsumoApi());
        $resultado = json_encode($result);
        $response = json_decode($resultado);
        $texto = '';
        foreach ($response as $key                  => $d)
        {
            $retorno = json_encode($response[$key]->municipio->microrregiao->mesorregiao->UF->nome); 
            $estado = json_decode($retorno);
            
           
            if($texto == '')
            {
                $texto = $estado;
            }
            else
            {
                $texto .= ',';
                $texto .= $estado;
            }
            
            
        }
        $array = explode(',', $texto);
        $tamanho = count($array);
        $estados = array_unique($array, $tamanho);
        return $estados;
    }
 */
    public function Cidade($request)
    {
        
        
        $result = json_decode($this->ConsumoApi($request));
        $resultado = json_encode($result);
        $response = json_decode($resultado);

        $array = array();
        $array2 = array();
        foreach ($response as $key                  => $d)
        {
                
                $municipio = json_encode($d->municipio->nome);
                $municipio = json_decode($municipio);
                $idMunicipio = json_encode($d->municipio->id); 
                $idMunicipio = json_decode($idMunicipio);
                array_push($array, $municipio);
                array_push($array2, $idMunicipio);
                $resultado = DB::select('select * from municipios where Nome = ?', [$municipio]);
                $cont = count($resultado);
                if($cont == 0)
                {
                    $data = ['Nome'                  => $municipio,'IdMunicipio'                  => $idMunicipio,'Estado'                  => $request]; 
                    $this->municipios->create($data);      
                }       
            
        }
        $tamanho = count($array);
        $tamanho2 = count($array2);
        $cidades = array_unique($array, $tamanho);
        $ids = array_unique($array2, $tamanho2);
        $response = ['Cidades'=> $cidades,'ids'=> $ids,'estado'=> $this->UfPorNome($request)]; 
        return $response;
         
    }

    public function UfPorNome($uf)
    {
        $uf = strtoupper($uf);
        $estados = array(
            'AC'=> 'Acre',
            'AL'=> 'Alagoas',
            'AP'=> 'Amapá',
            'AM'=> 'Amazonas',
            'BA'=> 'Bahia',
            'CE'=> 'Ceará',
            'DF'=> 'Distrito Federal',
            'ES'=> 'Espírito Santo',
            'GO'=> 'Goiás',
            'MA'=> 'Maranhão',
            'MT'=> 'Mato Grosso',
            'MS'=> 'Mato Grosso do Sul',
            'MG'=> 'Minas Gerais',
            'PA'=> 'Pará',
            'PB'=> 'Paraíba',
            'PR'=> 'Paraná',
            'PE'=> 'Pernambuco',
            'PI'=> 'Piauí',
            'RJ'=> 'Rio de Janeiro',
            'RN'=> 'Rio Grande do Norte',
            'RS'=> 'Rio Grande do Sul',
            'RO'=> 'Rondônia',
            'RR'=> 'Roraima',
            'SC'=> 'Santa Catarina',
            'SP'=> 'São Paulo',
            'SE'=> 'Serigipe',
            'TO'=> 'Tocantins'
        );

        $estado = $estados[$uf];

        return $estado;
    }
    public function NomePorUf($estado)
    {
        $estado = strtolower($estado);

        $uf = array(
        'acre'                      => 'AC',
        'alagoas'                      => 'AL',
        'amapá'                      => 'AP',
        'amazonas'                      => 'AM',
        'bahia'                      => 'BA',
        'ceará'                      => 'CE',
        'distrito federal'                      => 'DF',
        'espírito santo'                      => 'ES',
        'goiás'                      => 'GO',
        'maranhão'                      => 'MA',
        'mato grosso'                      => 'MT',
        'mato grosso do sul'                      => 'MS',
        'minas gerais'                       => 'MG',
        'pará'                    => 'PA',
        'paraíba'                      => 'PB',
        'paraná'                      => 'PR',
        'pernambuco'                      => 'PE',
        'piauí'                      => 'PI',
        'rio de janeiro'                       => 'RJ',
        'rio grande do norte'                      => 'RN',
        'rio grande do sul'                      => 'RS',
        'rondônia'                      => 'RO',
        'roraima'                      => 'RR',
        'santa catarina'                      => 'SC',
        'são paulo'                      => 'SP',
        'serigipe'                      => 'SE',
        'tocantins'                      => 'TO',
        );

        $estado = $uf[$estado];             

        return $estado;
    }


    public function validarUF($uf)
    {
        
        $uf = strtoupper($uf);
        //FALTA TERMINAR O IF
        if( $uf == 'AC' || 'AL' ||  'AP' ||  'AM' ||  'BA' ||  'CE' ||  'DF' ||  'ES' ||  'GO' ||  'MA'
         ||  'MT' ||  'MS' ||  'MG' ||  'PA' ||  'PB' ||  'PR' ||  'PE' ||  'PI' ||  'RJ' ||  'RN'
         ||  'RS' ||  'RO' ||  'RR' ||  'SC' ||  'SP' ||  'SE' ||  'TO' )
        {
            return true;
        }
        else
        {
            return false;
        }
    }

    public function validarEstado($estado)
    {
        $estado = strtolower($estado);
        $resultado = false;
        //FALTA TERMINAR O IF
        if( $estado == 'acre' || 'alagoas' || 'amapá' || 'amazonas' || 'bahia' || 'ceará' || 'distrito federal' || 'espírito santo' ||
         'goiás' || 'maranhão' || 'mato grosso' || 'mato grosso do sul' ||  'minas gerais'  || 'pará' || 'paraíba' || 'paraná' ||
          'pernambuco' || 'piauí' || 'rio de janeiro' || 'rio grande do norte' || 'rio grande do sul' || 'rondônia' ||
            'roraima' || 'santa catarina' || 'são paulo' || 'serigipe' || 'tocantins')
        {
            $resultado = true;
        }
        else
        {
            $resultado = false;
        }
        return $resultado;
    }
        
}
