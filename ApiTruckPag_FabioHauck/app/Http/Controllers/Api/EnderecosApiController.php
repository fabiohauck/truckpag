<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\MasterApiController;
use App\Models\Enderecos;
use App\Models\Municipios;

class EnderecosApiController  extends MasterApiController
{
    protected $enderecos;
    protected $municipios;
    protected $path = 'enderecos';  
    protected $Path2 = 'municipios';

    public function __construct(Enderecos $enderecos,Municipios $municipios, Request $request) {
		$this->enderecos = $enderecos;
		$this->municipios = $municipios;
        $this->request = $request;
	}

    
}
