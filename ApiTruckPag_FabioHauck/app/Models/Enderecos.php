<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Enderecos extends Model
{
    protected $fillable = [
        'Id','Cidade', 'Logradouro', 'Numero', 'Bairro', 
    ];

    public function regras()
    {
        return [
            'Id' => 'required|unique:Id',
           
        ];
    }

}