<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Municipios extends Model
{
    protected $fillable = [
        'Id','Nome', 'IdMunicipio', 'Estado',
    ];

    public function regras()
    {
        return [
            'Id' => 'required|unique:Id',
           
        ];
    }

}